#!/usr/bin/env python

import owi
import argparse
import sys

description = """Converts L2X path to nclight or nclight_L2M path."""
parser = argparse.ArgumentParser(description = description)

parser.add_argument("-p", "--paths", action="store", nargs="*", default=None, help="""Input string, containing a path to be converted into an Url.
If no string is given, the input string is taken on stdin (and can be multiple).
""")
parser.add_argument("-m", "--mode", action="store", default="sw", help="Product type : sw or gd or ll_gd")
parser.add_argument("-r", "--resolution", action="store", default=None, help="Resolution of returned product.")

args = parser.parse_args()

if args.paths is None:
    lines = sys.stdin.readlines()
    for l in lines:
        print(owi.L2CtoLight(l.strip(), resolution=args.resolution, mode=args.mode))
else:
    for l in args.paths:
        print(owi.L2CtoLight(l.strip(), resolution=args.resolution, mode=args.mode))

