#!/usr/bin/env python

import sys
import owi
import argparse
import datetime
import logging

logging.basicConfig()
logger = logging.getLogger(__name__)

if sys.gettrace():
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# Define the version of your script
SCRIPT_VERSION = owi.__version__

if __name__ == "__main__":
    description = """Converts nclight file to gridded nc format"""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--crs', action="store", default=None,
                        help="destination crs as proj4, e.g., 'epsg:4326'. If None, aeqd will be used")
    parser.add_argument('infile', help="Input file path")
    parser.add_argument('outfile', help="Output file path")
    # Add version argument
    parser.add_argument('-v', '--version', action='version', version=SCRIPT_VERSION)
    args = parser.parse_args()

    logger.info('{} -> {}'.format(args.infile, args.outfile))
    owi.nclight2gridded(args.infile, args.outfile, crs_out=args.crs)
