#!/usr/bin/env python

import sys
import owi
import argparse
import datetime
import logging

logging.basicConfig()
logger = logging.getLogger(__name__)

if sys.gettrace():
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# Define the version of your script
SCRIPT_VERSION = owi.__version__

if __name__ == "__main__":
    description = """Converts owi nc file to lightweight nc format"""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('infile', help="Input file path")
    parser.add_argument('outfile', help="Output file path")
    # Add version argument
    parser.add_argument('-v', '--version', action='version', version=f'{SCRIPT_VERSION}')
    args = parser.parse_args()

    # Rest of your code
    logger.info("read %s", args.infile)
    data = owi.readFile(args.infile)
    meta = owi.readMeta(args.infile)
    startdate = datetime.datetime.strptime(owi.getAttribute(args.infile,'firstMeasurementTime'),"%Y-%m-%dT%H:%M:%SZ")
    stopdate = datetime.datetime.strptime(owi.getAttribute(args.infile,'lastMeasurementTime'),"%Y-%m-%dT%H:%M:%SZ")
    owi.writeLight(args.outfile, data, startdate + (stopdate - startdate) / 2, meta=meta)
    logger.info("wrote %s", args.outfile)
