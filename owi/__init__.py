#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
from builtins import str
from builtins import range

from past.utils import old_div
import sys
import os
import datetime
import glob
import netCDF4
import numpy as np
import numpy.ma as ma
import logging
import warnings
from collections import OrderedDict
from ._version import __version__

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

version = __version__


def readFile(fname):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        data={}
        #list_var=('owiLon','owiLat','owiHeading','owiEcmwfWindSpeed','owiWindSpeed_IPF', 'owiPreProcessing/estimWindDir200', 'owiPreProcessing/MixWindDir200', 'owiPreProcessing/CoefMixWindDir200', 'owiEcmwfWindDirection','owiWindDirection_mod1','owiWindSpeed_mod1','owiWindDirection_mod2','owiWindSpeed_mod2','owiWindDirection_mod3','owiWindSpeed_mod3','owiPreProcessing/FilterBinary200','owiLandFlag','owiIncidenceAngle')
        try:
            nc=netCDF4.Dataset(fname)
        except  Exception as e:
            logger.warning('skipping unreadable %s : %s' % (fname, str(e)))
            return None
        
        # read owiLon to get valid shape
        data['owiLon']=nc.variables['owiLon'][::]
        refDims=nc.variables['owiLon'].dimensions
        # read groups
        for groupName in [ None ] + list(nc.groups.keys()):
            if groupName:
                group=nc.groups[groupName]
                groupNamePref="%s/" % groupName
            else:
                group=nc
                groupNamePref=""
                
            groupAttrs=group.ncattrs()
            for varName in group.variables:
                var=group[varName]
                dims=var.dimensions
                if dims == refDims:
                    data["%s%s" % (groupNamePref,varName)] = var[::]
                else:
                    # some vars are 3D: 
                    try:
                        if dims[1:] == refDims:
                            values3D=var[::]
                            for islice in range(np.shape(values3D)[0]):
                                value=values3D[islice,:,:]
                                try:
                                    # try to find sliceName from varName and ncattrs
                                    sliceName=group.getncattr([ groupAttr for groupAttr in groupAttrs if groupAttr in varName ][0]).split(', ')[islice]
                                except:
                                    sliceName="%d" % islice
                                data["%s%s/%s" % (groupNamePref,varName, sliceName)]=value 
                    except:
                        pass
        
        nc.close
    
    return data

def readMeta(fname):
    data={}
    #list_var=('owiLon','owiLat','owiHeading','owiEcmwfWindSpeed','owiWindSpeed_IPF', 'owiPreProcessing/estimWindDir200', 'owiPreProcessing/MixWindDir200', 'owiPreProcessing/CoefMixWindDir200', 'owiEcmwfWindDirection','owiWindDirection_mod1','owiWindSpeed_mod1','owiWindDirection_mod2','owiWindSpeed_mod2','owiWindDirection_mod3','owiWindSpeed_mod3','owiPreProcessing/FilterBinary200','owiLandFlag','owiIncidenceAngle')
    attrs = ['long_name', 'units', 'valid_range', 'flag_values','flag_meanings', '_FillValue']
    try:
        nc=netCDF4.Dataset(fname)
    except  Exception as e:
        logger.warning('skipping unreadable %s : %s' % (fname, str(e)))
        return None
    
    # read globals attr
    for attr in nc.ncattrs():
        data[attr]=nc.getncattr(attr)
    
    refDims=nc.variables['owiLon'].dimensions
            
    # read groups
    for groupName in [ None ] + list(nc.groups.keys()):
        if groupName:
            group=nc.groups[groupName]
            groupNamePref="%s/" % groupName
        else:
            group=nc
            groupNamePref=""
            
        groupAttrs=group.ncattrs()
        for varName in group.variables:
            var=group[varName]
            data["%s%s" % (groupNamePref,varName)]={}
            for _attr in attrs:
                try:
                    data["%s%s" % (groupNamePref,varName)][_attr]=getattr(var,_attr)
                except:
                    data["%s%s" % (groupNamePref,varName)][_attr]=None
            # compatibility old version
            data["%s%s" % (groupNamePref,varName)]['description'] = data["%s%s" % (groupNamePref,varName)]['long_name']

            dims=var.dimensions
            if dims != refDims:
                
                # some vars are 3D: 
                try:
                    if dims[1:] == refDims:
                        for islice in range(var.shape[0]):
                            try:
                                # try to find sliceName from varName and ncattrs
                                sliceName=group.getncattr([ groupAttr for groupAttr in groupAttrs if groupAttr in varName ][0]).split(', ')[islice]
                            except:
                                sliceName="%d" % islice
                            data["%s%s/%s" % (groupNamePref,varName, sliceName)]={}
                            for _attr in attrs:
                                try:
                                    data["%s%s" % (groupNamePref,varName)][_attr]=getattr(var,_attr)
                                except:
                                    data["%s%s" % (groupNamePref,varName)][_attr]=None
                            # compatibility old version
                            data["%s%s" % (groupNamePref,varName)]['description'] = data["%s%s" % (groupNamePref,varName)]['long_name']

                except:
                    pass
    
    nc.close

    return data


def writeLight(fname,data,date,meta=None):
    """lightweigth file format writer    
    specs from https://github.com/Unidata/EC-netCDF-CF/blob/master/swath/swath.adoc#image-swath
    xarray reader exemple:
      * only root group:
        xarray.open_dataset(fname).transpose().squeeze() ####.rename_dims({'X':'x','Y':'y'})
      * all groups:
        xarray.merge([xarray.open_dataset(args.outfile),xarray.open_dataset(args.outfile,group='preprocessing')])
      
      
    """
    names_mapping = OrderedDict([
        ('owiLon'    , 'lon'),
        ('owiLat'    , 'lat'),
        ('owiWindSpeed' , 'wind_speed'),
        ('owiWindSpeed_co', 'wind_speed_co'),
        ('owiWindSpeed_cross', 'wind_speed_cross'),
        ('owiWindDirection' , 'wind_from_direction'),
        ('owiWindDirection_co', 'wind_from_direction_co'),
        ('owiWindDirection_cross', 'wind_from_direction_cross'),
#        ('owiWindQuality' , 'quality_flag'),
        ('owiMask' , 'mask_flag'),
        ('owiWindFilter' , 'heterogeneity_mask'), 
        ('owiNrcs' , 'nrcs_co'),
        ('owiNrcs_cross' , 'nrcs_cross'),
        ('owiNesz', 'nesz_co'),
        ('owiNesz_cross', 'nesz_cross'),
        ('owiNesz_cross_flattened', 'nesz_cross_flattened'),
        ('owiDsig_cross', 'dsig_cross'),
        ('owiIncidenceAngle' , 'incidence_angle'),
        ('owiElevationAngle' , 'elevation_angle'),
        ('owiHeading', 'heading_angle'),
        ('owiAncillaryWindSpeed', 'ancillary_wind_speed'),
        ('owiAncillaryWindDirection', 'ancillary_wind_direction'),
        ("owiMask_Nrcs", "owiMask_Nrcs"),
        ("owiMask_Nrcs_cross", "owiMask_Nrcs_cross")
        #("owiZonalWindSpeed", "owiZonalWindSpeed"),
        #("owiMeridionalWindSpeed", "owiMeridionalWindSpeed"),
        #("owiUncertaintyDirection", "owiUncertaintyDirection"),
        ])
    # global attrs (None mean no name translation )
    global_attrs = OrderedDict([
        ('sourceProduct' , None ), 
        ('missionName', None ), 
        ('polarisation', 'polarization' ), 
        ('footprint' , None ),
        ('l2ProcessingUtcTime' , None ),
        ('xsar_version', 'version' ),
        ("xsarsea_version", None),
        ("owiAlgorithmVersion", "grdwindinversion_version"),
        ("gmf", None),
        ("ancillary_source", None),
        ("winddir_convention", None),
        ("l2ProcessingUtcTime", None),
        ("coverage", None),
        ("ipf", None),
        ("icid", None),
        ("aux_cal", None),
        ("aux_ins", None),
        ("aux_pp1", None),
        ("aux_cal_recal", None),
        ("aux_pp1_recal", None),
        ])
    attrs = ['long_name','description', 'units','valid_range','flag_values','flag_meanings']
    # overwrite meta
    meta_over = {
        'wind_speed' : {
            'units' : "m/s",
            'long_name' : "Ocean 10m Wind speed from co- and cross- polarization"
        },
        'lon' : {
            'standard_name' : 'longitude',
            'units'         : 'degrees_east'
        },
        'lat' : {
            'standard_name' : 'latitude',
            'units'         : 'degrees_north'
        },
        'wind_from_direction': {
            'long_name'     : 'Wind from direction (meteorologic convention)',
            'units'         : 'degrees',
#            'usage_level'   : "basic"
            }
    }
    # missing input will be forced to NaN/not implemented
    meta_not_implemented = {
        #'wind_streaks_orientation_stddev' : None
    }
    
    # translate dtype is only relevant for NETCDF4_CLASSIC
    translate_dtype = {
        np.dtype(np.uint8) : np.dtype(np.int16)
        }
    x,y = data['owiLon'].T.shape
    nc=netCDF4.Dataset(fname, 'w', format='NETCDF4_CLASSIC')
    nc.createDimension('time',1)
    nc.createDimension('y',y)
    nc.createDimension('x',x)
    nc.Conventions = 'CF-1.6'
    nc.title = 'SAR ocean surface wind field'
    nc.institution = 'IFREMER/CLS'
    nc.reference = 'Mouche Alexis, Chapron Bertrand, Knaff John, Zhao Yuan, Zhang Biao, Combot Clement (2019). Copolarized and Cross‐Polarized SAR Measurements for High‐Resolution Description of Major Hurricane Wind Structures: Application to Irma Category 5 Hurricane. Journal Of Geophysical Research-oceans, 124(6), 3905-3922. https://doi.org/10.1029/2019JC015056'
    nc.measurementDate = date.strftime("%Y-%m-%dT%H:%M:%SZ")

    setattr(nc, "nclight_processor_version", version)

    for global_attr in global_attrs.keys():
        map_global_attr = global_attrs[global_attr]
        if map_global_attr is None:
            map_global_attr = global_attr
        try:
            setattr(nc, map_global_attr, meta[global_attr])
        except:
            logger.warning('skipping missing attr %s' % global_attr)
    time = nc.createVariable('time',np.int32,dimensions=('time')) # np.int32 induce 2038 bug, but is compatible with nc3
    time.standard_name = 'time'
    epoch = datetime.datetime(1970, 1, 1)
    time.units = "seconds since %s" % epoch.strftime("%Y-%m-%d %H:%M:%S")
    time.calendar = "gregorian"
    #time[::]  = int(date.timestamp())
    time[::] = (date - epoch).total_seconds() # works for python < 3.3
    for in_name in names_mapping.keys():
        out_name = names_mapping[in_name]
        if in_name in data:
            value = data[in_name].T
            fill_value = data[in_name].fill_value
            # transpose dims for correct netcdf ordering
            dtype = value.dtype
            if dtype in translate_dtype:
                dtype = translate_dtype[dtype]
            ncvar = nc.createVariable(out_name,dtype,dimensions=('time','y','x'),fill_value=fill_value)
        
            for _attr in attrs:           
                try:
                    try:
                        setattr(ncvar,_attr,meta_over[in_name][_attr])
                    except:
                        if meta[in_name][_attr] is not None:
                            setattr(ncvar,_attr, meta[in_name][_attr])
                except:
                    pass
            ncvar[::]=value.T
        else:
            if out_name in meta_not_implemented:
                logger.warning('Forcing not implemented var %s' % out_name)
                ncvar = nc.createVariable(out_name,np.float32,dimensions=('time','y','x'),fill_value=np.nan)
                setattr(ncvar,"long_name","Not yet implemented")
            
            else:
                logger.warning('skipping missing var %s' % in_name)
                continue
        if in_name not in ['owiLon','owiLat']:
            ncvar.coordinates="time lat lon"

        if out_name in meta_over:
            for tag in meta_over[out_name]:
                ncvar.setncattr(tag, meta_over[out_name][tag])
        
    #unable to set crs for gdal     
    #crs = nc.createVariable("crs",int)
    #crs.grid_mapping_name = "latitude_longitude"
    #crs.semi_major_axis = 6378137 
    #crs.inverse_flattening = 298.2572235604902
    #crs.longitude_of_prime_meridian = 0.0

    nc.close()

    return 


def nclight2gridded(filein,fileout,crs_out=None):
    """
    if crs_out is None, each fileout will have his specific aeqd crs.
    if crs_out is not None, all fileout will share the same crs.
    """
    import numpy as np
    import xarray as xr
    import rioxarray as rxr
    import geo_shapely
    from pyproj.transformer import Transformer
    from shapely.geometry import Point
    from scipy import interpolate
    from scipy import ndimage
    import pyproj
    
    meta_over = {
        'x' : {
            'standard_name' : "projection_x_coordinate",
            'long_name' : 'Easting',
            'units'     : 'm'
            },
        'y' : {
            'standard_name' : "projection_y_coordinate",
            'long_name' : 'Northing',
            'units'     : 'm'
            },
        }
    
    ds_sw = xr.open_dataset(filein,mask_and_scale=False)
    x_center = ds_sw.x.size // 2
    y_center = ds_sw.y.size // 2
    
    lon_center = float(ds_sw.lon[0,y_center,x_center])
    lat_center = float(ds_sw.lat[0,y_center,x_center])

    # projection on aeqd . (will be intermediate crs if crs_out is not None)
    # get aeqd_crs at image center
    crs= geo_shapely.get_aeqd_crs(Point(lon_center,lat_center))
     
    # convert lon/lat in crs
    fromlonlat=Transformer.from_crs("epsg:4326",crs,always_xy=True) # always_xy=False is by default in pyproj 6+
    print("CRS %s" % crs)
    x_proj,y_proj=fromlonlat.transform(ds_sw.lon.values,ds_sw.lat.values)
    y_proj = y_proj.astype(float)
    x_proj = x_proj.astype(float)
    
    resolution = 1000 # in meters

    # compute grid size     
    x_size = int(np.ceil(np.max([-x_proj.min(),x_proj.max()])/resolution))*2+1
    y_size = int(np.ceil(np.max([-y_proj.min(),y_proj.max()])/resolution))*2+1
    
    
    # grid coordinates
    # do not use linspace : do it manualy, so computing inverse is easy
    #x = np.linspace(-(x_size-1)/2.,(x_size-1)/2.,num=x_size) * resolution
    #y = np.linspace(-(y_size-1)/2.,(y_size-1)/2.,num=y_size) * resolution
    x = (np.arange(x_size)-int((x_size-1)/2)) * resolution
    y = (np.arange(y_size)-int((y_size-1)/2)) * resolution
    # so given an x (resp y), in projection meters, we can have grid index with x / resolution
    x_idx = np.round((x_proj / resolution) + (x_size-1)/2 ).astype(int)
    y_idx = np.round((y_proj / resolution) + (y_size-1)/2 ).astype(int)
    
    # add x_proj and y_proj to original ds_sw, in valid coordinates (ie rounded at resolution)
    #ds_sw['x_proj'] = xr.DataArray(x_proj, dims=['time','Y', 'X'])
    #ds_sw['y_proj'] = xr.DataArray(y_proj, dims=['time','Y', 'X'])
    
    # get lon/lat for each grid cell
    #x_grid,y_grid = np.meshgrid(x,y)
    x_grid,y_grid = np.meshgrid(x,y)
    tolonlat=Transformer.from_crs(crs,"epsg:4326",always_xy=True)
    lon,lat=tolonlat.transform(x_grid,y_grid)
    
    # add time dim
    lon = lon.reshape([1] + list(lon.shape)).astype(np.float32)
    lat = lat.reshape([1] + list(lat.shape)).astype(np.float32)
    
    # generate dataarray
    da_lon = xr.DataArray(lon,coords=[ds_sw.time, y, x], dims=['time','y', 'x'])
    da_lat = xr.DataArray(lat,coords=[ds_sw.time, y, x], dims=['time','y', 'x'])
    
    # xarray dataset
    ds_gd = xr.Dataset({'lon' : da_lon,'lat' : da_lat})
    ds_gd.attrs = ds_sw.attrs
    # Here we could set a version to netcdf attrs, but nclight version is already set in writeLight,
    # and it would be the same, so we don't set one.


    # if resolution is too small, some gaps will occur in result
    # so we find gaps to be able to fill them later
    np_var = np.zeros(da_lon.shape[1:])
    # fill with 1 where the data will go
    np_var[y_idx,x_idx] = 1
    # mask is boolean grid where we need to interp
    mask = np_var == 0
    # xx and yy are all index
    xx, yy = np.meshgrid(np.arange(np_var.shape[1]), np.arange(np_var.shape[0]))
    # x_valid and y_valid are index of valid data
    #x_valid = xx[~mask]
    #y_valid = yy[~mask]
    # binary_closing and data to find isolated points that need interpolation
    mask_interp = ndimage.binary_closing(~mask) & mask
    # coordinates where interpolation is needed
    x_interp = xx[mask_interp]
    y_interp = yy[mask_interp]
    
    # variable loop
    for var in list((set(ds_sw.coords.keys()) | set(ds_sw.keys())).difference(set(['time']))):
        # set up dataarray. dtype is forced to np.float64, because we use Nan for interpolation
        #ds_sw[var].values.dtype
        try:
            fillvalue=ds_sw[var]._FillValue
        except:
            fillvalue=netCDF4.default_fillvals['f8']
        da_var = xr.DataArray(np.full(da_lon.shape, fillvalue, dtype=np.float64),coords=[ds_sw.time,y, x], dims=['time','y', 'x'])
        # fill dataarray
        da_var.values[0,y_idx,x_idx] = ds_sw[var].values
        # save nan, as they  are going to be interpolated
        nan_mask = np.isnan(da_var.values)
        da_var.values[nan_mask] = fillvalue # not really needed
        try:
            da_var.values[0,y_interp,x_interp] = np.nan
            da_var = da_var.ffill(dim='x',limit=2)
            da_var = da_var.ffill(dim='y',limit=2)
        except Exception as e:
            logging.warning("unable to fill nan on %s: %s" % (var,str(e)))
        # restore nan
        da_var.values[nan_mask] = np.nan
        # set original dtype
        da_var = da_var.astype(ds_sw[var].values.dtype)
        da_var.attrs = ds_sw[var].attrs
        da_var.rio.set_spatial_dims('x','y',inplace=True)
        ds_gd[var] = da_var

    encoding={}
    for var in list(set(ds_gd.coords.keys()) | set(ds_gd.keys())):
        encoding[var] = {}
        if var in meta_over:
            # python 3 only (merge dict )
            # ds_gd[var].attrs = { **ds_sw[var].attrs,**meta_over[var] }
            # python 2 compat:
            ds_gd[var].attrs.update(meta_over[var])
        if '_FillValue' not in ds_gd[var].attrs:
            # disable automatic fillvalue in fileout
            encoding[var].update({'_FillValue': None})

    ds_gd.rio.set_spatial_dims('x','y', inplace=True)
    ds_gd.rio.write_crs(crs, inplace=True)
    
    #encoding={'lat': {'_FillValue': None}, 'lon': {'_FillValue': None}}

    # final reprojection on crs_out
    if crs_out is not None:
        logger.info("reprojecting to final proj %s" % str(crs_out))
        ds_gd = ds_gd.rio.reproject(crs_out)
        if pyproj.CRS(crs_out).is_geographic :
            # crs out is lon/lat : rename x,y to lon,lat
            ds_gd = ds_gd.drop(['lon','lat']).rename({'x':'lon','y':'lat'})
            ds_gd['lon'].attrs = {
                "long_name" : "longitude",
                "standard_name" : "longitude",
                "units" : "degrees_east",
                }
            ds_gd['lat'].attrs = {
                "long_name" : "latitude",
                "standard_name" : "latitude",
                "units" : "degrees_north"
                }
            del encoding['x']
            del encoding['y']
            ds_gd.rio.set_spatial_dims('lon','lat', inplace=True)
            ds_gd.rio.write_crs(crs_out, inplace=True)

    ds_gd.to_netcdf(fileout, encoding=encoding)


def getAttribute(fname, attr):
    try:
        nc=netCDF4.Dataset(fname)
    except  Exception as e:
        logger.warning('skipping unreadable %s : %s' % (fname, str(e)))
        return None
    
    if attr in nc.ncattrs():
        return getattr(nc, attr)
    return None

def getFootprint(fname):
    try:
        nc=netCDF4.Dataset(fname)
    except  Exception as e:
        logger.warning('skipping unreadable %s : %s' % (fname, str(e)))
        return None
    
    if "footprint" in nc.ncattrs():
        return nc.footprint
    else:
        return None    
    
    
def getDateFromFname(fname):
    tmp=os.path.basename(fname).split('-')
    startDate=datetime.datetime.strptime(tmp[4],"%Y%m%dt%H%M%S")
    stopDate=datetime.datetime.strptime(tmp[5],"%Y%m%dt%H%M%S")
    date=startDate+old_div((stopDate-startDate),2)
    return date

def getDataAtCoord(data,lon,lat):
    import geo
    dataAtCoord={}
    #logger.info("lon : %s" % str(lon))
    landFlag=data['owiLandFlag']
    landFlag[landFlag>0]=1
    Mask = landFlag
    # Mask[Mask>0]=1
    # Mask1=data['owiLandFlag']
    # Mask2=np.logical_not(data['FilterBinary200'])
    lons = ma.array(data['owiLon'],mask=Mask)
    lats = ma.array(data['owiLat'],mask=Mask)
    # lons = ma.array(data['owiLon'],mask=Mask2)
    # lats = ma.array(data['owiLat'],mask=Mask2)
    # lons = data['owiLon']
    # lats = data['owiLat']
    dist,bearing=geo.haversine(lon, lat,lons, lats)
    mindist=np.min(dist)
    imindist=np.argmin(dist)
    if mindist > 20: #
        logger.warning("Buoy to far from sar image (%s km)" % mindist)
        return None
    data['dist_km']=mindist

    for key in data:
        try:
            dataAtCoord[key]=np.ravel(data[key])[imindist]
        except:
            dataAtCoord[key]=data[key]

        # convert dir in image conv to oceano convention
        # if ("Dir" in key) and (not key.startswith("DirectionModifie")) and (not key.startswith("Coef")) :
        if ("Dir200" in key) and (not key.startswith("DirectionModifie")) and (not key.startswith("Coef")) :
            if dataAtCoord[key] >180:
                dataAtCoord[key]=dataAtCoord[key]-180
            else :
                dataAtCoord[key]=dataAtCoord[key]+180
    return dataAtCoord


# Convert L2C file system path into L2M filesystem path
# Input validity is not tested. Supports converting raw L2X products paths or nclight product paths
# INPUT:
#     path : iterable or non iterable of a full L2C/nclight path (list of string or just string depending on input)
#     resolution : resolution of the L2M path needed
# Return:
#    A path or a list of path (same type as path parameter) containing the L2M paths
def L2CtoL2M(path, resolution):
    if type(path) == str:
        pathList = [path]
    else:
        pathList = path

    pathList = [p.replace("L2C", "L2M").replace("_CC_", "_CM_").replace("-cc-", "-cm-") for p in pathList]

    #pathList = [p.replace("nclight", "nclight_L2M").replace("-cc-", "-cm-") if "nclight" in p and "nclight_L2M" not in p else p for p in pathList]

    pathList = [p.split("/") for p in pathList]
    for p in pathList:
        filenameSplit = p[-1].split("-")
        # Set the field which indicates the file resolution to the needed resolution.
        # Adding leading zeros to keep field length
        filenameSplit[-2] = str(resolution).zfill(len(filenameSplit[-2]))
        p[-1] = "-".join(filenameSplit)
    pathList = ["/".join(p) for p in pathList]

    if type(path) == str:
        return pathList[0]
    else:
        return pathList


# Convert L2C file system path into L2C/L2M light filesystem path
# INPUT:
#     path (list of string or string) : iterable or non iterable of a full L2C path
#     resolution (int) : resolution of the L2M path needed. If not provided, will return the L2CLight
# Return:
#    A path or a list of path (same type as path parameter) containing the L2CLight/L2MLight paths
def L2CtoLight(path, resolution=None, mode='sw'):
    if type(path) == str:
        pathList = [path]
    else:
        pathList = path

    if not mode in ["sw", "ll_gd", "gd"]:
        raise ValueError(f"Invalid given mode : {mode}")

    pathsOut = []
    for p in pathList:
        base = os.path.dirname(p)
        # If requested a L2M and that input is L2C
        if resolution and "L2C" in p and "-cc" in p:
            addPath = f"post_processing/nclight_L2M_{mode}"
            newPath = os.path.join(base, addPath)
            newNcFile = os.path.splitext(os.path.basename(p))[0] + "_%s.nc" % mode
            fullNcPath = os.path.join(newPath, newNcFile)
            file = L2CtoL2M(fullNcPath, resolution)
        else:
            addPath = "post_processing/nclight"
            newPath = os.path.join(base, addPath)
            newNcFile = os.path.splitext(os.path.basename(p))[0] + "_%s.nc" % mode
            fullNcPath = os.path.join(newPath, newNcFile)
            file = fullNcPath
        pathsOut.append(file)

    if type(path) == str:
        return pathsOut[0]
    else:
        return pathsOut


def lightToL2C(path):
    if type(path) == str:
        pathList = [path]
    else:
        pathList = path

    pathsOut = []
    for p in pathList:
        base = os.path.dirname(p)
        base = "/".join(base.split("/")[:-2])
        id_sat = "-".join(os.path.basename(p).split("-")[4:6])
        newFile = glob.glob(os.path.join(base, "*%s*.nc" % id_sat))[0]
        pathsOut.append(newFile)

    if type(path) == str:
        return pathsOut[0]
    else:
        return pathsOut



