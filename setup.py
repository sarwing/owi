from setuptools import setup

setup(name='owi',
      description='Owi netCDF reader',
      url='https://gitlab.ifremer.fr/sarwing/owi.git',
      author="Theo Cevaer",
      author_email="Theo.Cevaer@ifremer.fr",
      license='GPL',
      use_scm_version={'write_to': '%s/_version.py' % "owi"},
      setup_requires=['setuptools_scm'],
      packages=['owi'],
      zip_safe=False,
      scripts=['bin/owi2nclight.py', 'bin/nclight2gridded.py', 'bin/L2Ctolight.py'],
      install_requires=['numpy',
                        'netCDF4',
                        'future',
                        "bottleneck",
                        "xarray",
                        "rioxarray",
                        "geo_shapely",
                        "pyproj",
                        "shapely",
                        "scipy"]
      )
